# hiennguyen-20240425

## Test Question No. 1

## Test Question No. 2

Given an array of stock prices, where the value at each index represents the price of a stock on a specific day, implement a function `calculateMaxProfit` to determine the maximum profit that can be achieved by buying and selling the stock on different days.

 

- The function should take an array of stock prices as input.

- It should return the maximum profit that can be obtained by buying one unit of stock on a specific day and selling it on a future day.

- If no profit can be achieved, the function should return 0.

 

Example:

const stockPriceList = [2, 3, 6, 4, 3];

calculateMaxProfit(stockPriceList); // Should return 4

#### Run the program 
- Run : node calculateMaxProfit.js
- Run test: npm test 