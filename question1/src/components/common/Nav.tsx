import React, { FC, ReactNode } from 'react';
import { Toolbar, Typography, Button, Box } from '@mui/material';

interface NavProps {
  children?: ReactNode;
}

const Nav: FC<NavProps> = ({ children }) => {
  return (
    <Toolbar>
      <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
        Coin Search
      </Typography>

      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <Button color='inherit'>Home</Button>
        <Button color='inherit'>News</Button>
        <Button color='inherit'>Trending</Button>
        {children}
      </Box>
    </Toolbar>
  );
};

export default Nav;
