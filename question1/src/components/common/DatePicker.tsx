import { Box } from '@mui/material';
import React, { FC } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

interface DateRangePickerProps {
  startDate: Date;
  setStartDate: (date: Date) => void;
  endDate: Date;
  setEndDate: (date: Date) => void;
}

const DateRangePicker: FC<DateRangePickerProps> = ({
  startDate,
  setStartDate,
  endDate,
  setEndDate,
}) => {
  return (
    <Box
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        margin: '0 8px',
      }}
    >
      <Box style={{ marginRight: '10px' }}>
        <label htmlFor='startDate'>Start: </label>
        <DatePicker
          id='startDate'
          selected={startDate}
          onChange={(date: Date) => setStartDate(date)}
          dateFormat='MM/dd/yyyy'
          selectsStart
          startDate={startDate}
          endDate={endDate}
        />
      </Box>
      <Box>
        <label htmlFor='endDate'>End: </label>
        <DatePicker
          id='endDate'
          selected={endDate}
          onChange={(date: Date) => setEndDate(date)}
          dateFormat='MM/dd/yyyy'
          selectsEnd
          startDate={startDate}
          endDate={endDate}
          minDate={startDate}
        />
      </Box>
    </Box>
  );
};

export default DateRangePicker;
