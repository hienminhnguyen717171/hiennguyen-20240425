import React, { FC } from 'react';
import { Autocomplete, CircularProgress, TextField } from '@mui/material';
import useFetchListCoin from '../../custom-hooks/useFetchListCoin';
import { I_Coin_List } from '@src/constant/type';
import DateRangePicker from './DatePicker';

interface SearchInputProps {
  onChange: (value: I_Coin_List) => void;
  onChangeStartDate: (date: Date) => void;
  onChangeEndDate: (date: Date) => void;
  startDate: Date;
  endDate: Date;
}

const SearchInput: FC<SearchInputProps> = ({
  onChange,
  onChangeStartDate,
  onChangeEndDate,
  startDate,
  endDate,
}) => {
  const { data: coinList, loading } = useFetchListCoin();

  return (
    <>
      <Autocomplete
        disablePortal
        id='combo-box-demo'
        size='small'
        options={coinList}
        isOptionEqualToValue={(option: I_Coin_List, value: I_Coin_List) =>
          option.name === value.name
        }
        getOptionLabel={(option: I_Coin_List) => option.name}
        getOptionKey={(option: I_Coin_List) => option.id}
        onChange={(_, newValue: I_Coin_List) => {
          onChange(newValue);
        }}
        sx={{ width: 300 }}
        renderInput={(params) => (
          <TextField
            {...params}
            label='Search Coins here'
            InputProps={{
              ...params.InputProps,
              endAdornment: (
                <React.Fragment>
                  {loading ? (
                    <CircularProgress color='inherit' size={20} />
                  ) : null}
                  {params.InputProps.endAdornment}
                </React.Fragment>
              ),
            }}
          />
        )}
      />
      <DateRangePicker
        startDate={startDate}
        setStartDate={onChangeStartDate}
        endDate={endDate}
        setEndDate={onChangeEndDate}
      />
    </>
  );
};

export default SearchInput;
