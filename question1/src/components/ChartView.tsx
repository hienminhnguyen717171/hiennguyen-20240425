import React, { FC } from 'react';
import { Box, Button, Grid, Paper, Typography } from '@mui/material';
import useFetchCoinDetail from '@src/custom-hooks/useFetchCoinDetail';
import { I_Coin_Detail, I_Coin_List } from '@src/constant/type';
import { ErrorOutline } from '@mui/icons-material';
import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  Tooltip,
  XAxis,
  YAxis,
} from 'recharts';
import useFetchCoinHistory from '@src/custom-hooks/useFetchCoinHistory';

interface ChartViewProps {
  coin: I_Coin_List;
  startDate: Date;
  endDate: Date;
}
interface CoinDetailsProps {
  coin: I_Coin_Detail;
  price: [string, string][];
  marketCap: [string, string][];
  totalVolumes: [string, string][];
}
const CoinDetails: FC<CoinDetailsProps> = ({ coin, price, totalVolumes }) => {
  // Prepare the data for the line chart
  const chartDataPrice = price.map((dataPoint: any) => ({
    date: new Date(dataPoint[0]).toDateString(),
    price: dataPoint[1],
  }));

  const currentPrice = chartDataPrice?.[chartDataPrice.length - 1]?.price;

  const chartDataVolume = totalVolumes.map((dataPoint: any) => ({
    date: new Date(dataPoint[0]).toDateString(),
    volume: dataPoint[1],
  }));
  const priceChangeColor = coin.price_change_24h < 0 ? 'red' : 'green';

  return (
    <Box className='coin-details'>
      <h2>
        {coin.name} ({coin.symbol.toUpperCase()})
      </h2>
      <Box
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          width: '100%',
        }}
      >
        <span style={{ color: priceChangeColor, fontSize: '0.8em' }}>
          {coin.price_change_24h > 0 && '+'}
          {coin.price_change_24h?.toFixed(2)}%
        </span>
        <Typography style={{ fontSize: '2em', margin: 0 }}>
          Market Cap Rank: #{coin?.market_cap_rank}
        </Typography>
        <Typography style={{ fontSize: '2em', margin: 0 }}>
          Current value: ${currentPrice?.toFixed(2)}
        </Typography>
      </Box>
      <h2>Historical Data</h2>
      <LineChart width={500} height={300} data={chartDataPrice}>
        <CartesianGrid strokeDasharray='3 3' />
        <XAxis dataKey='date' />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type='monotone' dataKey='price' stroke='#8884d8' r={0} />
      </LineChart>

      <LineChart width={500} height={300} data={chartDataVolume}>
        <CartesianGrid strokeDasharray='3 3' />
        <XAxis dataKey='date' />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line type='monotone' dataKey='volume' stroke='#8884d8' r={0} />
      </LineChart>
    </Box>
  );
};

const ChartView: FC<ChartViewProps> = ({ coin, startDate, endDate }) => {
  const { data: coinDetail } = useFetchCoinDetail(coin?.id ?? '');
  const { price, marketCap, totalVolumes } = useFetchCoinHistory(
    coin?.id,
    startDate,
    endDate,
  );

  return (
    <Grid container spacing={2}>
      {/* Left section for coin details */}
      <Grid item xs={4}>
        <Paper
          style={{
            padding: '20px',
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <Box>
            <Typography variant='h6'>Coin Detail</Typography>
          </Box>

          {coinDetail ? (
            <Box
              style={{
                padding: '20px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <img
                src={coinDetail?.image?.small}
                alt={coinDetail?.name}
                style={{ width: '80px', height: '80px', marginBottom: '10px' }}
              />
              <Typography variant='h6'>{coin?.name}</Typography>
              <Typography variant='subtitle1' color='textSecondary'>
                {coinDetail?.symbol}
              </Typography>
              {/* Add more details as needed */}

              <Box style={{ marginTop: '10px' }}>
                {/* Render each community data with icon */}
                <Box>
                  <Typography variant='subtitle1'>
                    Twitter Followers:
                  </Typography>
                  <Typography variant='body1'>
                    {coinDetail?.community_data?.twitter_followers || (
                      <ErrorOutline />
                    )}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='subtitle1'>Facebook Likes:</Typography>
                  <Typography variant='body1'>
                    {coinDetail?.community_data?.facebook_likes || (
                      <ErrorOutline />
                    )}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='subtitle1'>
                    Reddit Subscribers:
                  </Typography>
                  <Typography variant='body1'>
                    {coinDetail?.community_data?.reddit_subscribers || (
                      <ErrorOutline />
                    )}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='subtitle1'>
                    Telegram channel user
                  </Typography>
                  <Typography variant='body1'>
                    {coinDetail?.community_data
                      ?.telegram_channel_user_count || <ErrorOutline />}
                  </Typography>
                </Box>
                <Box>
                  <Typography variant='subtitle1'>Twitter Followers</Typography>
                  <Typography variant='body1'>
                    {coinDetail?.community_data?.twitter_followers || (
                      <ErrorOutline />
                    )}
                  </Typography>
                </Box>
              </Box>
              <Button
                variant='contained'
                color='primary'
                style={{ marginTop: '20px' }}
              >
                Add to Favorites
              </Button>
            </Box>
          ) : (
            <ErrorOutline fontSize='large' />
          )}
        </Paper>
      </Grid>
      {/* Right section for coin chart */}
      <Grid item xs={8}>
        <Paper style={{ padding: '20px' }}>
          <Typography variant='h6'>Coin Chart</Typography>
          {coinDetail ? (
            <Box
              style={{
                padding: '20px',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              {coinDetail && (
                <CoinDetails
                  coin={coinDetail}
                  price={price}
                  marketCap={marketCap}
                  totalVolumes={totalVolumes}
                />
              )}
            </Box>
          ) : (
            <ErrorOutline fontSize='large' />
          )}
        </Paper>
      </Grid>
    </Grid>
  );
};

export default ChartView;
