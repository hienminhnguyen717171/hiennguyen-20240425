import React from 'react';
import { createRoot } from 'react-dom/client';
import MainPage from './pages/MainPage';

// Say something
console.log('[ERWT] : Renderer execution started');

// Application to Render
const app = <MainPage />;

// Render application in DOM
createRoot(document.getElementById('app')).render(app);
