import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://api.coingecko.com/api/v3', // Set your base URL here
  timeout: 5000, // Set a timeout
  headers: {
    'Content-Type': 'application/json', // Set default content type
  },
});

// Request interceptor for handling errors
instance.interceptors.request.use(
  (config) => {
    // Modify request config before sending
    return config;
  },
  (error) => {
    // Handle request errors
    return Promise.reject(error);
  },
);

// Response interceptor for handling errors
instance.interceptors.response.use(
  (response) => {
    // Handle successful responses
    return response;
  },
  (error) => {
    // Handle response errors
    return Promise.reject(error);
  },
);

export default instance;
