import { I_Coin_List } from '@src/constant/type';
import useFetchList from './common/useFetchList';

const useFetchListCoin = () => {
  const { data, loading, error } = useFetchList<I_Coin_List[]>('/coins/list');
  return { data, loading, error };
};

export default useFetchListCoin;
