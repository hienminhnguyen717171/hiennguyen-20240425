import { useState, useEffect } from 'react';
import { AxiosResponse } from 'axios';
import axios from '../../config/axiosConfig';

function useListFetch<T>(url: string, params?: string) {
  const [data, setData] = useState<T>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const abortController = new AbortController();
    const signal = abortController.signal;

    const fetchData = async () => {
      if (params === '') {
        return;
      }
      try {
        setLoading(true);
        const response: AxiosResponse<T> = await axios.get(url, { signal });
        setData(response.data);
      } catch (error) {
        if (error.name === 'AbortError') {
          // Request was aborted, no need to handle error
          return;
        }
        setError(error.message);
      } finally {
        setLoading(false);
      }
    };

    fetchData();

    return () => {
      abortController.abort(); // Cancel the request when component unmounts or when a new request is made
    };
  }, [url]);

  return { data, loading, error };
}

export default useListFetch;
