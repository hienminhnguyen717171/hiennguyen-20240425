import { api_key } from '@src/constant/type';
import { useEffect, useState } from 'react';
import axios from '../config/axiosConfig';

const useFetchCoinHistory = (
  coinId: string,
  startDate: Date,
  endDate: Date,
) => {
  const [loading, setLoading] = useState(true);
  const [price, setPrice] = useState([]);
  const [marketCap, setMarketCap] = useState([]);
  const [totalVolumes, setTotalVolumes] = useState([]);

  useEffect(() => {
    const handleCoinSelect = (coinId: string) => {
      const queryParams = new URLSearchParams();
      queryParams.append('vs_currency', 'usd');
      queryParams.append(
        'from',
        Math.floor(startDate.getTime() / 1000).toString(),
      );
      queryParams.append('to', Math.floor(endDate.getTime() / 1000).toString());

      setLoading(true);

      axios
        .get(`/coins/${coinId}/market_chart/range?${queryParams.toString()}`)
        .then((response) => {
          const price = response.data.prices;
          const marketCaps = response.data.market_caps;
          const totalVolumes = response.data.total_volumes;

          setPrice(price);
          setMarketCap(marketCaps);
          setTotalVolumes(totalVolumes);
        })
        .catch((error) => {
          console.error('Error fetching historical data', error);
        });
    };
    if (coinId) {
      handleCoinSelect(coinId);
    }
  }, [coinId, startDate, endDate]);
  return { loading, price, marketCap, totalVolumes };
};
export default useFetchCoinHistory;
