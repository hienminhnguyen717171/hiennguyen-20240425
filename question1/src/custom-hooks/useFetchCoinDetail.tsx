import useFetchList from './common/useFetchList';

const useFetchCoinDetail = (id: string) => {
  const { data, loading, error } = useFetchList<any>(`/coins/${id}`, id);
  return { data, loading, error };
};

export default useFetchCoinDetail;
