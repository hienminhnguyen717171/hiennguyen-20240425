import React, { useState, ChangeEvent } from 'react';
import { Container, Box } from '@mui/material';
import Nav from '@src/components/common/Nav';
import SearchInput from '@src/components/common/SearchInput';
import ChartView from '@src/components/ChartView';
import { I_Coin_List } from '@src/constant/type';

function SearchPage() {
  const [searchTerm, setSearchTerm] = useState<I_Coin_List>(null);
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const handleChangeSearchInput = (value: I_Coin_List) => {
    setSearchTerm(value);
  };

  const handleChangeStartDate = (date: Date) => {
    setStartDate(date);
  };

  const handleChangeEndDate = (date: Date) => {
    setEndDate(date);
  };

  return (
    <div>
      <Box position='static'>
        <Nav>
          <SearchInput
            onChangeStartDate={handleChangeStartDate}
            onChangeEndDate={handleChangeEndDate}
            onChange={handleChangeSearchInput}
            startDate={startDate}
            endDate={endDate}
          />
        </Nav>
      </Box>
      <Container sx={{ paddingTop: 4 }}>
        <ChartView coin={searchTerm} startDate={startDate} endDate={endDate} />
      </Container>
    </div>
  );
}

export default SearchPage;
