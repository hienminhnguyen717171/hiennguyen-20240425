# Project Readme: React Search Coin

## Description
React Search Coin is a simple React application designed to help users search for information about various cryptocurrencies. Users can search for specific coins and view their details, including price, market cap, volume, and more.

## Core Features

- Search Functionality: Users can search for cryptocurrencies by name or symbol.
- Coin Details: View detailed information about each cryptocurrency, including current price, market cap, volume, and more.
- Responsive Design: The application is designed to work seamlessly across desktop, tablet, and mobile devices.

## Tech 

- ⚛️ React (Hook)
- 🌀 TypeScript
- 🥗 SASS/SCSS Loader
- 🛶 LESS Loader (optional)
- 🎨 CSS Loader
- 📸 Image Loader
- 🆎 Font Loader
- 🧹 ESLint
- 🔱 Webpack & Configuration
- 🧩 Aliases for Project Paths
- 🔥 React Fast Refresh + Webpack HMR

<br />

## Installation

#### To install this boilerplate you need to run following commands

<br>

Clone the repository :

<br>

Install dependencies using Yarn or NPM or PNPM :

# using pnpm
pnpm install

# or using yarn
yarn install

# or using npm
npm install

<br />

## Start : Development

To develop and run your web application, you need to run following command :

yarn start

<br />

## Lint : Development

To lint application source code using ESLint via this command :

yarn lint

<br />

## Build : Production

Distribution files output will be generated in dist/ directory by default.

To build the production ready files for distribution, use the following command :

yarn build

<br />

## Serve : Production

Serve helps you serve a static site, single page application or just a static file. It also provides a neat interface for listing the directory's contents. This command serves build files from dist/ directory.

yarn serve

<br />

## Webpack Configurations

To make it easier for managing environment based webpack configurations, we using separated development and production configuration files, they are available in :

# Development webpack config
tools/webpack/webpack.config.dev.js

# Production webpack config
tools/webpack/webpack.config.prod.js

For further information, you can visit [Webpack Configuration](https://webpack.js.org/configuration/)
