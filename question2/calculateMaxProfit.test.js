const calculateMaxProfit = require('./calculateMaxProfit');

describe('calculateMaxProfit', () => {
    it('returns the maximum profit for a given array of stock prices', () => {
        const stockPriceList = [2, 3, 6, 4, 3];
        expect(calculateMaxProfit(stockPriceList)).toEqual(4);
    });

    it('returns 0 if no profit can be achieved', () => {
        const stockPriceList = [6, 5, 4, 3, 2];
        expect(calculateMaxProfit(stockPriceList)).toEqual(0);
    });

    it('returns 0 if the array is empty', () => {
        const stockPriceList = [];
        expect(calculateMaxProfit(stockPriceList)).toEqual(0);
    });

    it('returns 0 if there is only one price in the array', () => {
        const stockPriceList = [10];
        expect(calculateMaxProfit(stockPriceList)).toEqual(0);
    });

    it('returns 0 if all prices are the same', () => {
        const stockPriceList = [3, 3, 3, 3, 3];
        expect(calculateMaxProfit(stockPriceList)).toEqual(0);
    });

    it('returns the maximum profit when prices are in descending order', () => {
        const stockPriceList = [10, 8, 6, 4, 2];
        expect(calculateMaxProfit(stockPriceList)).toEqual(0);
    });
});
