function calculateMaxProfit(stockPrices) {
    let maxProfit = 0;
    let minPrice = Infinity;

    for (let i = 0; i < stockPrices.length; i++) {
        // Update the minimum price seen so far
        if (stockPrices[i] < minPrice) {
            minPrice = stockPrices[i];
        }
        // Calculate the profit if we sell at the current price
        // and update the maximum profit if necessary
        else if (stockPrices[i] - minPrice > maxProfit) {
            maxProfit = stockPrices[i] - minPrice;
        }
    }

    return maxProfit;
}

module.exports = calculateMaxProfit;
